# smartRM bash utility #

SmartRM package is improved version of trash bin.

### RM mode ###

Move files to trash bin:
```
$ smartrm filename1 filename2
```

Move files to custom trash specified in user config file:
```
$ smartrm filename1 filename2 --config path_to_user_config
```

Move empty directory to trash bin:
```
$ smartrm -d dirname
$ smartrm --directory dirname
```

Move not empty directory to trash bin:
```
$ smartrm -r dirname
$ smartrm --recursive dirname
```

Move file in force mode:
```
$ smartrm -f filename
$ smartrm --force filename
```


### TRASH mode ###

Show files that already in trash bin:
```
$ smartrash
$ smartrash --show
```

Restore files from trash bin:
```
$ smartrash -r filename1 filename2
$ smartrash --recover filename1 filename2
```

Permanently remove files from trash bin:
```
$ smartrash -c filename1 filename2
$ smartrash --clean filename1 filename2
```


### CONFIG mode ###


Run program with user config:
```
$ smartconfig --config-file path_to_config
```

To set or reset storage period policy:
```
$ smartconfig --period-policy days
$ smartconfig --period
```

To set or reset capacity policy:
```
$ smartconfig --capacity-policy megabytes
$ smartconfig --capacity
```

To set or reset amount policy:
```
$ smartconfig --amount-policy days
$ smartconfig --amount
```

To start with your path to trash bin:
```
$ smartconfig --trash-path path_to_trash
```

To print config:
```
$ smartconfig
$ smartconfig --show
```

### Other program flags ###

#### Silent-Mode ####
To run program without any promts and printing current processes
Use flag ```--silent```

#### Dry-Run ####
To run program without any literal changes
Show only preliminary program result
Use flag ```--dry-run```


### Run with applying custom policies ###

To run program with your policies use flags ```--period-policy```, ```--capacity-policy```, ```--amount-policy```, ```--trash_path```

```
$ smartrm filename1 filename2 --capacity-policy megabytes
```
```
$ smartrash -r filename1 filename2 --trash-path path_to_trash
```