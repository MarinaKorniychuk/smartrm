#!/usr/env/bin python2.7
# -*- coding: utf-8 -*-

import os
import sys
import pytest
import shutil

import smartrm
import tests.filenames_getters as getters
import smartrm.constants as constants

PATH_TO_TEST_DIR = os.path.join(os.getcwd(), '.smartrm_test_dir')
CUSTOM_CONFIG_PATH = os.path.join(os.path.expanduser('~'), '.smartrm_test_config.json')
CUSTOM_PATH_TO_TRAH = os.path.join(os.path.expanduser('~'), '.smartrm-test')
CUSTOM_CONFIG = dict(
    path_to_trash=CUSTOM_PATH_TO_TRAH,
    period_policy_days=10,
    capacity_policy_mb=1024,
    amount_policy=200
)
FILE_PATHS = getters.get_filepaths()
FILE_NAMES = getters.get_filenames()
DIR_PATHS = getters.get_dirpaths()
DIR_NAMES = getters.get_dirnames()

def setup_module(module):
    if os.path.exists(constants.CONFIG_PATH_DEFAULT):
        os.remove(constants.CONFIG_PATH_DEFAULT)

    if os.path.exists(constants.TRASH_PATH_DEFAULT):
        shutil.rmtree(constants.TRASH_PATH_DEFAULT)

    if not os.path.exists(PATH_TO_TEST_DIR):
        os.makedirs(PATH_TO_TEST_DIR)


def teardown_module(module):
    shutil.rmtree(PATH_TO_TEST_DIR)
    if os.path.exists(CUSTOM_PATH_TO_TRAH):
        shutil.rmtree(CUSTOM_PATH_TO_TRAH)


def setup_function(function):
    for file_path in FILE_PATHS:
        with open(file_path, 'a'):
            pass
    smartrm.rm.remove_files(FILE_PATHS)

    from smartrm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)


def teardown_function(function):
    for file_path in os.listdir(PATH_TO_TEST_DIR):
        full_path = os.path.join(PATH_TO_TEST_DIR, file_path)
        if os.path.isfile(full_path):
            os.remove(full_path)
        else:
            shutil.rmtree(full_path)
    smartrm.trash.clean_files([])
    if os.path.exists(CUSTOM_PATH_TO_TRAH):
        shutil.rmtree(CUSTOM_PATH_TO_TRAH)


# =========== Tests ===========


def test_clean_simpe():
    smartrm.trash.clean_files(FILE_NAMES)
    files_in_trash = getters.get_trash_filenames()
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)


def test_clean_dry():
    smartrm.trash.clean_files(FILE_NAMES, dry_run=True)
    trash_files = getters.get_trash_filenames()
    assert all(file_name in trash_files for file_name in FILE_NAMES)


def test_restore():
    smartrm.trash.restore_files(FILE_NAMES)
    files_in_trash = getters.get_trash_filenames()
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)
    assert all(os.path.exists(filepath) for filepath in FILE_PATHS)


def test_restore_dry():
    FILE_PATHS = getters.get_filepaths()
    FILE_NAMES = getters.get_filenames()
    smartrm.trash.restore_files(FILE_NAMES, dry_run=True)
    files_in_trash = getters.get_trash_filenames()
    assert all(not os.path.exists(filepath) for filepath in FILE_PATHS)
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)


def test_clean_all():
    file_path = FILE_PATHS[0]
    file_name = FILE_NAMES[0]
    with open(file_path, 'a'):
        pass
    smartrm.rm.remove_files([file_path])

    trash_base = smartrm.trash_base.load_trash_base()
    files_with_name = [file_dict for file_dict in trash_base
                           if file_dict['name'] == file_name]
    assert len(files_with_name) == 2

    smartrm.trash.clean_files([file_name])
    files_in_trash = getters.get_trash_filenames()
    assert file_path not in files_in_trash


def test_restore_all():
    file_path = FILE_PATHS[0]
    file_name = FILE_NAMES[0]
    with open(file_path, 'a'):
        pass
    smartrm.rm.remove_files([file_path])

    trash_base = smartrm.trash_base.load_trash_base()
    files_with_name = [file_dict for file_dict in trash_base
                           if file_dict['name'] == file_name]
    assert len(files_with_name) == 2

    smartrm.trash.restore_files([file_name])
    files_in_trash = getters.get_trash_filenames()
    assert file_path not in files_in_trash
    assert os.path.exists(file_path)
    assert os.path.exists(file_path + ' (1)')


def test_restore_with_adding_suf():
    file_path = FILE_PATHS[0]
    file_name = FILE_NAMES[0]
    with open(file_path, 'a'):
        pass
    smartrm.rm.remove_files([file_path])

    trash_base = smartrm.trash_base.load_trash_base()
    files_with_name = [file_dict for file_dict in trash_base
                           if file_dict['name'] == file_name]
    assert len(files_with_name) == 2

    smartrm.trash.restore_files([file_name])
    files_in_trash = getters.get_trash_filenames()
    assert file_name not in files_in_trash
    assert os.path.exists(file_path)
    assert os.path.exists(file_path + ' (1)')


def test_restore_with_replace():
    file_path = FILE_PATHS[0]
    file_name = FILE_NAMES[0]
    with open(file_path, 'a'):
        pass
    smartrm.rm.remove_files([file_path])

    trash_base = smartrm.trash_base.load_trash_base()
    files_with_name = [file_dict for file_dict in trash_base
                           if file_dict['name'] == file_name]
    assert len(files_with_name) == 2

    smartrm.trash.restore_files([file_name], replace=True, add_suffix=False)

    files_in_trash = getters.get_trash_filenames()
    assert file_name not in files_in_trash
    assert os.path.exists(file_path)
    assert not os.path.exists(file_path + ' (1)')


def test_restore_with_suffix_files_with_suffix_exist():
    file_path = FILE_PATHS[0]
    file_name = FILE_NAMES[0]
    new_file = [file_path]
    existing_files = [file_path + ' ({})'.format(index)
                         for index in range(1, 10)]
    new_file += existing_files
    for filepath in new_file:
        with open(filepath, 'a'):
            pass
    smartrm.trash.restore_files(FILE_NAMES)
    files_in_trash = getters.get_trash_filenames()
    assert file_path not in files_in_trash
    assert os.path.exists(file_path + ' (10)')


def test_restore_when_original_dir_not_exists():
    file_name = 'new_file'
    dir_path = getters.get_dirpaths()[0]
    os.makedirs(dir_path)
    new_file = os.path.join(dir_path, file_name)
    with open(new_file, 'a'):
        pass
    smartrm.rm.remove_files([new_file])
    shutil.rmtree(dir_path)
    assert not os.path.exists(dir_path)
    smartrm.trash.restore_files([file_name])
    files_in_trash = getters.get_trash_filenames()
    assert file_name not in files_in_trash
    assert os.path.exists(new_file)


def test_clean_with_invalid_argument():
    file_path = FILE_PATHS[0]
    with pytest.raises(TypeError):
        smartrm.trash.clean_files(file_path)


def test_restore_with_invalid_argument():
    file_path = FILE_PATHS[0]
    with pytest.raises(TypeError):
        smartrm.trash.restore_files(file_path)


def test_restore_with_invalid_flags():
    with pytest.raises(ValueError):
        smartrm.trash.restore_files([], add_suffix=True, replace=True)


def test_clean_with_custom_config():
    from smartrm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)

    smartrm.trash.restore_files(FILE_NAMES)

    smartrm.rm.remove_files(FILE_PATHS, custom_trash_path=CUSTOM_CONFIG['path_to_trash'])

    files_in_trash = getters.get_trash_filenames_by_path_to_config(CUSTOM_CONFIG_PATH)
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)
    assert len(os.listdir(CUSTOM_PATH_TO_TRAH)) == 10

    smartrm.trash.clean_files(FILE_NAMES, custom_trash_path=CUSTOM_PATH_TO_TRAH)

    files_in_trash = getters.get_trash_filenames_by_path_to_config(CUSTOM_CONFIG_PATH)
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)


def test_clean_with_custom_config_cfg():

    custom_cfg_config = '/home/marina/.config.cfg'
    smartrm.trash.restore_files(FILE_NAMES)

    smartrm.rm.remove_files(FILE_PATHS, custom_trash_path=CUSTOM_PATH_TO_TRAH, path_to_config=custom_cfg_config)

    files_in_trash = getters.get_trash_filenames_by_path_to_trash(CUSTOM_PATH_TO_TRAH)
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)
    assert len(os.listdir(CUSTOM_PATH_TO_TRAH)) == 10

    smartrm.trash.clean_files(FILE_NAMES, custom_trash_path=CUSTOM_PATH_TO_TRAH, path_to_config=custom_cfg_config)

    files_in_trash = getters.get_trash_filenames_by_path_to_config(custom_cfg_config)
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)


def test_restore_with_custom_config():
    smartrm.trash.restore_files(FILE_NAMES)
    smartrm.rm.remove_files(FILE_PATHS, path_to_config=CUSTOM_CONFIG_PATH)

    files_in_trash = getters.get_trash_filenames_by_path_to_config(CUSTOM_CONFIG_PATH)
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)
    assert len(os.listdir(CUSTOM_PATH_TO_TRAH)) == 10

    smartrm.trash.restore_files(FILE_NAMES, path_to_config=CUSTOM_CONFIG_PATH)

    files_in_trash = getters.get_trash_filenames_by_path_to_config(CUSTOM_CONFIG_PATH)
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)


def test_restore_with_custom_config_Cfg():

    custom_cfg_config = '/home/marina/.config.cfg'
    smartrm.trash.restore_files(FILE_NAMES)
    smartrm.rm.remove_files(FILE_PATHS, custom_trash_path=CUSTOM_PATH_TO_TRAH, path_to_config=custom_cfg_config)

    files_in_trash = getters.get_trash_filenames_by_path_to_trash(CUSTOM_PATH_TO_TRAH)
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)
    assert len(os.listdir(CUSTOM_PATH_TO_TRAH)) == 10

    smartrm.trash.restore_files(FILE_NAMES, custom_trash_path=CUSTOM_PATH_TO_TRAH, path_to_config=custom_cfg_config)

    files_in_trash = getters.get_trash_filenames_by_path_to_config(custom_cfg_config)
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)


def test_clean_with_edited_config():
    FILE_PATHS = getters.get_filepaths()
    FILE_NAMES = getters.get_filenames()
    smartrm.trash.restore_files(FILE_NAMES)
    smartrm.rm.remove_files(FILE_PATHS, custom_trash_path=CUSTOM_CONFIG['path_to_trash'])

    files_in_trash = getters.get_trash_filenames_by_path_to_trash(CUSTOM_CONFIG['path_to_trash'])
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)
    assert len(os.listdir(CUSTOM_CONFIG['path_to_trash'])) == 10

    smartrm.trash.clean_files(FILE_NAMES, custom_trash_path=CUSTOM_CONFIG['path_to_trash'])

    files_in_trash = getters.get_trash_filenames_by_path_to_trash(CUSTOM_CONFIG['path_to_trash'])
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)


def test_restore_with_edited_config():
    FILE_PATHS = getters.get_filepaths()
    FILE_NAMES = getters.get_filenames()
    smartrm.trash.restore_files(FILE_NAMES)
    smartrm.rm.remove_files(FILE_PATHS, custom_trash_path=CUSTOM_CONFIG['path_to_trash'])

    files_in_trash = getters.get_trash_filenames_by_path_to_trash(CUSTOM_PATH_TO_TRAH)
    assert all(file_name in files_in_trash for file_name in FILE_NAMES)
    assert len(os.listdir(CUSTOM_PATH_TO_TRAH)) == 10

    smartrm.trash.restore_files(FILE_NAMES, custom_trash_path=CUSTOM_CONFIG['path_to_trash'])

    files_in_trash = getters.get_trash_filenames_by_path_to_trash(CUSTOM_PATH_TO_TRAH)
    assert all(file_name not in files_in_trash for file_name in FILE_NAMES)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)


# ========== Tests for policies ==========


def test_capacity_policy():

    smartrm.config.set_capacity_policy(1)
    smartrm.trash.clean_files([])
    FILE_PATHS = getters.get_filepaths()[5:]
    FILE_NAMES = getters.get_filenames()[5:]

    for file_path in FILE_PATHS:
        with open(file_path, 'a') as out_file:
            out_file.write('.'*300000)

    smartrm.rm.remove_files(FILE_PATHS)

    files_in_trash = getters.get_trash_filenames()
    assert len(files_in_trash) == 3


def test_amout_policy():

    files_in_trash = getters.get_trash_filenames()
    assert len(files_in_trash) == 9

    smartrm.config.set_amount_policy(3)
    smartrm.trash.print_files_from_trash()
    files_in_trash = getters.get_trash_filenames()
    assert len(files_in_trash) == 3
    smartrm.config.set_amount_policy(100)


def test_amout_policy_with_CUSTOM_CONFIG():

    FILE_PATHS = getters.get_filepaths()
    FILE_NAMES = getters.get_filenames()

    smartrm.config.print_config()
    smartrm.trash.restore_files(FILE_NAMES)
    smartrm.rm.remove_files(FILE_PATHS, path_to_config=CUSTOM_CONFIG_PATH)

    smartrm.config.set_amount_policy(3, path_to_config=CUSTOM_CONFIG_PATH)
    smartrm.trash.print_files_from_trash(path_to_config=CUSTOM_CONFIG_PATH)
    files_in_trash = getters.get_trash_filenames_by_path_to_config(CUSTOM_CONFIG_PATH)
    print files_in_trash
    assert len(files_in_trash) == 3
