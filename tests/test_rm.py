#!/usr/env/bin python2.7
# -*- coding: utf-8 -*-

import os
import sys
import pytest
import shutil

import smartrm
import tests.filenames_getters as getters
import smartrm.constants as constants

PATH_TO_TEST_DIR = os.path.join(os.getcwd(), '.smartrm_test_dir')
FILE_PATHS = getters.get_filepaths()
FILE_NAMES = getters.get_filenames()
DIR_PATHS = getters.get_dirpaths()
DIR_NAMES = getters.get_dirnames()

def setup_module(module):
    if os.path.exists(constants.CONFIG_PATH_DEFAULT):
        os.remove(constants.CONFIG_PATH_DEFAULT)

    if os.path.exists(constants.TRASH_PATH_DEFAULT):
        shutil.rmtree(constants.TRASH_PATH_DEFAULT)

    if not os.path.exists(PATH_TO_TEST_DIR):
        os.makedirs(PATH_TO_TEST_DIR)


def teardown_module(module):
    if os.path.exists(PATH_TO_TEST_DIR):
        shutil.rmtree(PATH_TO_TEST_DIR)


def setup_function(function):
    for file_path in getters.get_filepaths():
        with open(file_path, 'a'):
            pass
    for dir_path in getters.get_dirpaths():
        os.makedirs(dir_path)


def teardown_function(function):
    for file_path in os.listdir(PATH_TO_TEST_DIR):
        full_path = os.path.join(PATH_TO_TEST_DIR, file_path)
        if os.path.isfile(full_path):
            os.remove(full_path)
        else:
            shutil.rmtree(full_path)

    smartrm.trash.clean_files([])


# =========== Tests ===========

def test_remove_simple():
    smartrm.rm.remove_files(FILE_PATHS)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(filename in files_in_trash for filename in FILE_NAMES)

def test_remove_dry_run():
    print getters.get_trash_filenames()
    smartrm.rm.remove_files(FILE_PATHS, dry_run=True)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(filename not in files_in_trash for filename in FILE_NAMES)


def test_remove_by_regexp():
    smartrm.rm.remove_by_regexp('filename[89]', PATH_TO_TEST_DIR)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS[:7])
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS[7:])
    files_in_trash = getters.get_trash_filenames()
    assert all(filename not in files_in_trash for filename in FILE_NAMES[:7])
    assert all(filename in files_in_trash for filename in FILE_NAMES[7:])


def test_remove_dry_by_pattern():
    smartrm.rm.remove_by_regexp('file_[123]', PATH_TO_TEST_DIR, dry_run=True)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(filename not in files_in_trash for filename in FILE_NAMES)


def test_remove_empty_dirs():
    smartrm.rm.remove_files(DIR_PATHS)
    assert all(not os.path.exists(dir_path) for dir_path in DIR_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(dir_name in files_in_trash for dir_name in DIR_NAMES)


def test_remove_empty_dir_raise_errors():
    result = smartrm.rm.remove_files(
        [DIR_PATHS[0]],
        rm_directory=False,
        rm_recursive=False
    )
    assert result['errors'] == True


def test_remove_non_empty_dir_without_recursive_flag():
    dir_path = getters.get_dirpaths()[0]
    dir_name = getters.get_dirnames()[0]
    with open(os.path.join(dir_path, 'file'), 'a'):
        pass
    result = smartrm.rm.remove_files(
            [dir_name],
            rm_recursive=False
    )
    assert result['errors'] == True


def test_remove_non_empty_dir():
    open(os.path.join(DIR_PATHS[0], 'file'), 'a').close()
    smartrm.rm.remove_files(DIR_PATHS)
    assert all(not os.path.exists(dir_path) for dir_path in DIR_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(dir_name in files_in_trash for dir_name in DIR_NAMES)


def test_remove_force():
    for file_path in FILE_PATHS:
        os.chmod(file_path, 0444)
    smartrm.rm.remove_files(FILE_PATHS)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(filename in files_in_trash for filename in FILE_NAMES)


def test_remove_with_custom_config():
    from smartrm.config import _write_config as write_config
    custom_config_path = os.path.join(os.path.expanduser('~'), '.smartrm_test_config.json')
    path_to_trash = os.path.join(os.path.expanduser('~'), '.smartrm-test')
    custom_config = dict(
        path_to_trash=path_to_trash,
        period_policy_days=10,
        capacity_policy_mb=1024,
        amount_policy=200,
        cfg=False
    )
    write_config(custom_config_path, custom_config)
    smartrm.rm.remove_files(FILE_PATHS, path_to_config=custom_config_path)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames_by_path_to_config(custom_config_path)
    assert all(filename in files_in_trash for filename in FILE_NAMES)


def test_remove_with_custom_config_cfg():
    custom_cfg_config = '/home/marina/.config.cfg'
    smartrm.rm.remove_files(FILE_PATHS, path_to_config=custom_cfg_config)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames_by_path_to_config(custom_cfg_config)
    assert all(filename in files_in_trash for filename in FILE_NAMES)


def test_remove_with_custom_trash_path():
    path_to_trash = '/home/marina/.smartrm-test'
    smartrm.rm.remove_files(FILE_PATHS, custom_trash_path=path_to_trash)

    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames_by_path_to_trash(path_to_trash)
    assert all(filename in files_in_trash for filename in FILE_NAMES)


def test_remove_by_regexp_with_invalid_regexp():
    with pytest.raises(ValueError):
        smartrm.rm.remove_by_regexp(')name', PATH_TO_TEST_DIR)


def test_remove_by_pattern_with_nonexisting_dir():
    with pytest.raises(OSError):
        smartrm.rm.remove_by_regexp(
            'file',
            os.path.join(PATH_TO_TEST_DIR, 'directory')
        )


def test_remove_with_partial_nonexistence():
    result = smartrm.rm.remove_files(FILE_PATHS + ['nonexistent_file'])
    assert result['errors'] == True
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    files_in_trash = getters.get_trash_filenames()
    assert all(filename in files_in_trash for filename in FILE_NAMES)
    assert 'nonexistent_file' not in files_in_trash


def test_remove_with_invalid_argument():
    with pytest.raises(TypeError):
        smartrm.rm.remove_files(getters.get_dirpaths()[0])

