#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os

import smartrm
import filenames_getters as getters

PATH_TO_TEST_DIR = os.path.join(os.getcwd(), '.smartrm_test_dir')


def get_trash_filenames():
    file_dicts = smartrm.trash.get_files()
    filenames = [file_dict['name'] for file_dict in file_dicts]
    return filenames


def get_trash_filenames_by_path_to_config(path_to_config):
    file_dicts = smartrm.trash.get_files(path_to_config=path_to_config)
    filenames = [file_dict['name'] for file_dict in file_dicts]
    return filenames


def get_trash_filenames_by_path_to_trash(trash_path):
    file_dicts = smartrm.trash.get_files(trash_path=trash_path)
    filenames = [file_dict['name'] for file_dict in file_dicts]
    return filenames


def get_filepaths():
    file_names = ['filename' + str(index) for index in range(1, 10)]
    file_paths = [os.path.join(PATH_TO_TEST_DIR, file_name)
              for file_name in file_names]
    return file_paths

def get_dirpaths():
    dir_names = ['dirname' + str(index) for index in range(1, 10)]
    dir_paths = [os.path.join(PATH_TO_TEST_DIR, dir_name)
              for dir_name in dir_names]
    return dir_paths

def get_filenames():
    return ['filename' + str(index) for index in range(1, 10)]


def get_dirnames():
    return ['dirname' + str(index) for index in range(1, 10)]

