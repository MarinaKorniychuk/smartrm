#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import pytest
import os
import shutil
import sys

import smartrm
import smartrm.constants as constants
import tests.filenames_getters as  getters


def setup_module(module):
    if not os.path.exists(PATH_TO_TEST_DIR):
        os.makedirs(PATH_TO_TEST_DIR)


def teardown_module(module):
    shutil.rmtree(PATH_TO_TEST_DIR)


def setup_function(function):
    if os.path.exists(constants.CONFIG_PATH_DEFAULT):
        os.remove(constants.CONFIG_PATH_DEFAULT)

PATH_TO_TEST_DIR = os.path.join(os.getcwd(), '.smartrm_test_dir')
CUSTOM_CONFIG_PATH = os.path.join(os.path.expanduser('~'), '.smartrm_test_config.json')
CUSTOM_PATH_TO_TRASH = os.path.join(os.path.expanduser('~'), '.smartrm-test')
CUSTOM_CONFIG = dict(
    path_to_trash=CUSTOM_PATH_TO_TRASH,
    period_policy_days=10,
    capacity_policy_mb=1024,
    amount_policy=200
)


# ========== Tests ==========


def test_original_trash_not_exist():
    path_to_trash = smartrm.config.get_trash_path()

    if os.path.exists(path_to_trash):
        shutil.rmtree(path_to_trash)

    with pytest.raises(OSError):
        smartrm.config.move_trash(CUSTOM_CONFIG['path_to_trash'])


def test_moving_trash_to_the_same_dir():
    with pytest.raises(OSError):
        smartrm.config.move_trash(smartrm.config.get_trash_path())


def test_moving_trash_to_non_empty_dir():
    file_path = getters.get_filepaths()[0]

    with open(file_path, 'a'):
        pass

    with pytest.raises(OSError):
        smartrm.config.move_trash(PATH_TO_TEST_DIR)


def test_moving_to_dir_with_nonexisting_parent():
    with pytest.raises(OSError):
        smartrm.config.move_trash(os.path.join(PATH_TO_TEST_DIR, 'dir', 'sub_dir'))


def test_load_not_existing_config():
    with pytest.raises(OSError):
        smartrm.config.get_trash_path(CUSTOM_CONFIG_PATH + '.')


def test_load_dir_as_config():
    with pytest.raises(OSError):
        smartrm.config.get_trash_path(PATH_TO_TEST_DIR)


def test_load_invalid_config():
    file_path = getters.get_filepaths()[0]

    with open(file_path, 'a'):
        pass

    with pytest.raises(ValueError):
        smartrm.config.get_trash_path(file_path)


def test_moving_trash():
    file_path = getters.get_filepaths()[0]
    with open(file_path, 'a'):
        pass
    smartrm.rm.remove_files([file_path])
    smartrm.config.move_trash(CUSTOM_CONFIG['path_to_trash'])
    assert smartrm.config.get_trash_path() == CUSTOM_CONFIG['path_to_trash']
    assert os.path.exists(CUSTOM_CONFIG['path_to_trash'])
    assert not os.path.exists(constants.TRASH_PATH_DEFAULT)


def test_custom_configuratios():
    from smartrm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)

    smartrm.config.set_period_policy(
        value=100,
        path_to_config=CUSTOM_CONFIG_PATH
    )
    smartrm.config.set_capacity_policy(
        value=100,
        path_to_config=CUSTOM_CONFIG_PATH
    )
    smartrm.config.set_amount_policy(
        value=100,
        path_to_config=CUSTOM_CONFIG_PATH
    )

    custom_policies = smartrm.config.get_policies_values(CUSTOM_CONFIG_PATH)

    for policy in custom_policies:
        assert custom_policies[policy] == 100


def test_set_policies_functions():
    smartrm.config.set_period_policy(CUSTOM_CONFIG['period_policy_days'])
    smartrm.config.set_capacity_policy(CUSTOM_CONFIG['capacity_policy_mb'])
    smartrm.config.set_amount_policy(CUSTOM_CONFIG['amount_policy'])

    custom_policies = smartrm.config.get_policies_values()

    for policy in custom_policies:
        assert custom_policies[policy] == CUSTOM_CONFIG[policy]


def test_set_zero_valiues_for_policies():

    smartrm.config.set_period_policy(0)
    smartrm.config.set_capacity_policy(0)
    smartrm.config.set_amount_policy(0)

    custom_policies = smartrm.config.get_policies_values()

    for policy in custom_policies:
        assert custom_policies[policy] is None
