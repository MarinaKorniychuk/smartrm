# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os.path import join, dirname

import smartrm.constants as constants

setup(
    name=constants.PROGRAM_NAME,
    version='1.0',
    description='bash utility for removing files and directory',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='Marina Korniychuk',
    author_email='kornij4uk@gmail.com',
    url='https://bitbucket.org/MarinaKorniychuk/smartrm',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['smartrm = smartrm.main_funcs:main_rm',
                            'smartrash = smartrm.main_funcs:main_trash',
                            'smartconfig = smartrm.main_funcs:main_config']
    }
)
