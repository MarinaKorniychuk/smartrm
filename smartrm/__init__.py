# /usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""SmartRM package is improved version of trash bin.
It is possible to recover removed files.
All settings are easy to set.

Move files to trash bin:
    smartrm.rm.remove_files(['filename1', 'filename2'])

Move files to custom trash bin specified in user config file:
    smartrm.rm.remove_files(['name1', 'name2'], path_to_config='path_to_config')

Move files to custom trash bin:
    smartrm.rm.remove_files(['name1', 'name2'], custom_trash_path='path_to_trash')

Other params:
    dry_run - to chose dry-run mode;
    rm_force - to remove in force mode;
    silent_mode - to remove in silent mode;
    rm_directory - to remove empty directory;
    rm_recursive - to remove non empty directory recursively;
    rm_interactive - to remove in interactive mode;

Restore files from trash bin:
    smartrm.trash.restore_files(['filename1', 'filename2'])

Clean files from trash bin:
    smartrm.trash.clean_files(['filename1', 'filename2'])

To show files that already in thash bin:
    smartrm.trash.print_files_from_trash()

Other params:
    select_all - chose all files if there are several files with same name
in trash bin;
    replace - automaticly replace existing file with same name while restoring;
    add_suffix - automaticly add suffix to filename if file with same name
already exists;

To set policies: period_policy, capacity_policy, amount_policy
    """

import smartrm.rm as rm
import smartrm.trash as trash
import smartrm.config as config
import smartrm.logger as logger

# call program logger creation.
logger.create_logger()
