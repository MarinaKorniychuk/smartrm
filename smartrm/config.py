# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Module for config logic."""
import os
import json
import logging
import ConfigParser

import smartrm.logger as logger
import smartrm.constants as constants


def move_trash(
        new_path_to_trash,
        silent_mode=True,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Move trash bin to new path.
    Args:
        - new_path_to_trash (str): path to new trash bin location;
        - silent_mode (bool): if True, program run without any output;
        - path_to_config (str): path to config file;

    Raises:
        ValueError: if invalid config file passed;
        OSError:
            - if trash bin is already in new_path_to_trash;
            - if parent directory of new_path_to_trash does not exist;
            - if original trash bin does not exist;
            - if new_path_to_trash is not empty;
            - if not existing coonfig passed;

    """
    logger.set_logger_handlers(silent_mode)
    config_dict = load_config(path_to_config)

    old_path = config_dict['path_to_trash']
    new_path = os.path.realpath(new_path_to_trash)
    new_dir = os.path.dirname(new_path)

    if old_path == new_path:
        raise OSError('trash is in {} already'.format(new_path))

    if not os.path.exists(new_dir):
        message = 'parent directory {} does not exist'.format(new_dir)
        raise OSError(message)

    if os.path.exists(new_path) and any(os.listdir(new_path)):
        raise OSError('directory {} is not empty'.format(new_path))

    if not os.path.exists(old_path):
        raise OSError('trash does not exist')

    move_trashbin(old_path, new_path)

    config_dict['path_to_trash'] = new_path
    _write_config(path_to_config, config_dict)

    message = 'moving trash to {}'.format(new_path)
    logging.info(message)


def move_trashbin(old_trashbin_path, new_trashbin_path):
    os.rename(old_trashbin_path, new_trashbin_path)


# =============== Setters for policies ===============

def set_amount_policy(
        value,
        silent_mode=True,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Set amount policy.

    Args:
        value (int): max amount of elements in trash bin allowed;
        path_to_config (str): path to config file;
        silent_mode (bool): if True, program run without any output;

    Raises:
        ValueError:
            - if invalid config file passed;
            - if value is invalid.
        OSError: if not existing config file passed;
    """
    _set_policy_value(
        key='amount_policy',
        value=value,
        silent_mode=silent_mode,
        path_to_config=path_to_config)
    message = 'setting amount policy: {}'.format(value)
    logging.info(message)


def set_capacity_policy(
        value,
        silent_mode=True,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Set capacity policy.

    Args:
        value (int): max size of trash bin allowed (in Mb);
        silent_mode (bool): if True, program run without any output;
        path_to_config (str): path to config file;

    Raises:
        ValueError:
            - if invalid config file passed;
            - if value is invalid.
        OSError: if not existing config file passed;
    """
    _set_policy_value(
        key='capacity_policy_mb',
        value=value,
        silent_mode=silent_mode,
        path_to_config=path_to_config)
    message = 'setting capacity policy: {}'.format(value)
    logging.info(message)


def set_period_policy(
        value,
        silent_mode=True,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Set period policy.

    Args:
        value (int): period after files are removed (in days);
        silent_mode (bool): if True, program run without any output;
        path_to_config (str): path to config file;

    Raises:
        ValueError:
            - if invalid config file passed;
            - if value is invalid.
        OSError: if not existing config file passed;
    """
    _set_policy_value(
        key='period_policy_days',
        value=value,
        silent_mode=silent_mode,
        path_to_config=path_to_config)
    message = 'setting period policy: {}'.format(value)
    logging.info(message)


def _set_policy_value(
        key,
        value,
        silent_mode,
        path_to_config):
    """Set passed value to policy by passed key."""
    logger.set_logger_handlers(silent=silent_mode)
    config_dict = load_config(path_to_config)

    if not isinstance(value, int):
        raise ValueError('wrong type of policy value')

    if value < 0:
        raise ValueError('policy value is not valid')

    if value == 0:
        value = None

    config_dict[key] = value

    _write_config(path_to_config, config_dict)


# ============= Work with config data =============

def update_config(config_dict, **kwargs):
    """Update config with passed values.
    Args:
        - config dict (dict): dict trash wil be updated;
        - **kwargs: key-value pairs to be added to dict;
    """
    for key in kwargs:
        if kwargs[key] is not None:
            config_dict[key] = kwargs[key]


def _create_config(path_to_config):
    """Initialize config with default value.
    Args:
        - path_to_config (str): path to config file;
    """
    config_dict = constants.INIT_CONFIG
    config_dict['cfg'] = False
    _write_config(path_to_config, config_dict)


def _read_config(path_to_config):
    """Implement loading data from json config.

    Return:
        dict: dict with config parametrs;
    """
    if not path_to_config:
        path_to_config = constants.CONFIG_PATH_DEFAULT
    with open(path_to_config, 'r') as config_file:
        config_data = json.load(config_file)
    return config_data


def _read_cfg_config(path_to_config):
    """Implement loading data from cfg config.

    Return:
        dict: dict with config parametrs;
    """
    section_name = 'Settings'
    config = ConfigParser.ConfigParser()
    try:
        config.read(path_to_config)
        config_cfg_dict = dict(config.items(section_name))
        for policy in constants.POLICIES:
            config_cfg_dict[policy] = int(config_cfg_dict[policy])
        return config_cfg_dict
    except ConfigParser.NoSectionError:
        return dict()


def _write_config(path_to_config, config_dict):
    """Save config dict in json file."""
    if len(config_dict) == 4:
        config_dict['cfg'] = False
    if config_dict['cfg']:
        _write_cfg_config(path_to_config, config_dict)
    else:
        config_dict.pop('cfg')
        with open(path_to_config, 'w') as outfile:
            json.dump(config_dict, outfile, indent=4)


def _write_cfg_config(path_to_config, config_cfg_dict):
    """Save config dict in cfg file."""
    section_name = 'Settings'
    config_cfg_dict.pop('cfg')
    config = ConfigParser.RawConfigParser()
    config.add_section(section_name)
    for key, value in config_cfg_dict.iteritems():
        config.set(section_name, key, value)
    with open(path_to_config, 'wb') as outfile:
        config.write(outfile)


def load_config(path_to_config):
    """Load config data from file

    Args:
        - path_to_config (str): path to config file;

    Raises:
        ValueError:
            - if invalid config file passed;
        OSError:
            - if path to directory passed;
            - if not existing config file passed;
    """
    if os.path.isdir(path_to_config):
        raise OSError('{} is a directory'.format(path_to_config))

    if not os.path.exists(path_to_config):
        if path_to_config != constants.CONFIG_PATH_DEFAULT:
            raise OSError('{} does not exist'.format(path_to_config))

        directory = os.path.dirname(path_to_config)
        if not os.path.exists(directory):
            os.makedirs(directory)

        _create_config(path_to_config)

    try:
        config_dict = _read_config(path_to_config)
        config_dict['cfg'] = False
    except ValueError:
        config_dict = _read_cfg_config(path_to_config)
        config_dict['cfg'] = True

    _validate_config(config_dict)

    return config_dict


def _validate_config(config_dict):
    """Validate config for neccessary fields and their values."""
    if 'path_to_trash' not in config_dict:
        raise ValueError('path to trash must be specified in config file')

    for policy in constants.POLICIES:
        if policy in config_dict:
            value = config_dict[policy]
            if value is None:
                continue
            if not isinstance(value, int):
                raise ValueError('wrong type of policy value')

            if value < 0:
                raise ValueError('policy value is not valid')

            if value == 0:
                config_dict[policy] = None
        else:
            config_dict[policy] = None


# =============== Funcs for printing config ===============

def print_config(path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Show config data to user.

    Args:
        - path_to_config (str): path to config file;

    Raises:
        ValueError: if invalid config file passed;
        OSError: if not existing config file passed;
    """
    config_dict = load_config(path_to_config)
    _print_configarutions(
        amount_policy=config_dict['amount_policy'],
        capacity_policy_mb=config_dict['capacity_policy_mb'],
        period_policy_days=config_dict['period_policy_days'],
        path_to_trash=config_dict['path_to_trash']
    )


def _print_configarutions(**kwargs):
    """Print passed configuration values."""
    for key in kwargs:
        if kwargs[key] is None:
            kwargs[key] = 0
        print '{:19} {}'.format(_string_formatter(key), kwargs[key])


def _string_formatter(string):
    """Formate string from snake case to readible format."""
    string = string[0].upper() + string[1:]
    return ' '.join(string.split('_'))


# ============== Test functions ==============

def get_trash_path(path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Return path to current trash bin.

    Args:
        - path_to_config (str): path to config file;

    Raises:
        ValueError: if invalid config file passed;
        OSError: if not existing config file passed;

    Returns:
        str: path to trash bin;
    """
    config_dict = load_config(path_to_config)
    return config_dict['path_to_trash']


def get_policies_values(path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Return dict with all policies values.
    Args:
        - path_to_config (str): path to config file;

    Raises:
        ValueError: if invalid config file passed;
        OSError: if not existing config file passed;

    Returns:
        dict: current policies;
    """
    config_dict = load_config(path_to_config)
    return dict(
        amount_policy=config_dict['amount_policy'],
        period_policy_days=config_dict['period_policy_days'],
        capacity_policy_mb=config_dict['capacity_policy_mb']
    )
