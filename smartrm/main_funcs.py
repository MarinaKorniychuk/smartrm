# /usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""Handlers for main functions when start with command line."""

import logging

import smartrm.rm as rm
import smartrm.trash as trash
import smartrm.config as config
import smartrm.arg_parsers as arg_parsers


def main_rm(arguments=None):
    """Start program in rm mode with passed arguments.

    Args:
        arguments: parsed command line arguments;

    Returns:
        int: exit code;
    """
    arguments = arg_parsers.rm_parse_args()

    if arguments.files:
        try:
            rm.remove_files(
                filenames=arguments.files,
                dry_run=arguments.dry_run,
                rm_force=arguments.rm_force,
                silent_mode=arguments.silent_mode,
                rm_directory=arguments.rm_directory,
                rm_recursive=arguments.rm_recursive,
                rm_interactive=arguments.rm_interactive,
                custom_trash_path=arguments.trash_path,
                period_policy=arguments.period,
                capacity_policy=arguments.capacity,
                amount_policy=arguments.amount,
                path_to_config=arguments.config
            )
        except OSError as error:
            logging.error(error)
            return 1

    if arguments.regexp:
        try:
            rm.remove_by_regexp(
                pattern=arguments.regexp[0],
                subtree=arguments.regexp[1],
                dry_run=arguments.dry_run,
                rm_force=arguments.rm_force,
                silent_mode=arguments.silent_mode,
                rm_directory=arguments.rm_directory,
                rm_recursive=arguments.rm_recursive,
                rm_interactive=arguments.rm_interactive,
                custom_trash_path=arguments.trash_path,
                period_policy=arguments.period,
                capacity_policy=arguments.capacity,
                amount_policy=arguments.amount,
                path_to_config=arguments.config
            )
        except (OSError, ValueError) as error:
            logging.error(error)
            return 1

    return 0


def main_trash(arguments=None):
    """Start program in trash mode with passed arguments.

    Args:
        arguments: parsed command line arguments;

    Returns:
        int: exit code;
    """
    arguments = arg_parsers.trash_parse_args()

    is_show_only = arguments.clean is None and arguments.restore is None

    if not arguments.silent_mode and is_show_only or arguments.show:
        try:
            trash.print_files_from_trash(
                custom_trash_path=arguments.trash_path,
                path_to_config=arguments.config
            )
        except OSError as error:
            logging.error(error)
            return 1

    if arguments.clean is not None:
        try:
            trash.clean_files(
                filenames=arguments.clean,
                interactive=arguments.rm_interactive,
                select_all=arguments.select_all,
                dry_run=arguments.dry_run,
                silent_mode=arguments.silent_mode,
                path_to_config=arguments.config,
                custom_trash_path=arguments.trash_path,
                amount_policy=arguments.amount,
                capacity_policy=arguments.capacity,
                period_policy=arguments.period
            )
        except OSError as error:
            logging.error(error)
            return 1

    if arguments.restore is not None:
        try:
            trash.restore_files(
                filenames=arguments.restore,
                interactive=arguments.rm_interactive,
                dry_run=arguments.dry_run,
                select_all=arguments.select_all,
                replace=arguments.replace,
                add_suffix=arguments.add_suffix,
                silent_mode=arguments.silent_mode,
                path_to_config=arguments.config,
                custom_trash_path=arguments.trash_path,
                amount_policy=arguments.amount,
                capacity_policy=arguments.capacity,
                period_policy=arguments.period
            )
        except OSError as error:
            logging.error(error)
            return 1

    return 0


def main_config(arguments=None):
    """Start program in config mode with passed arguments.

    Args:
        arguments: parsed command line arguments;

    Returns:
        int: exit code;
    """
    arguments = arg_parsers.config_parse_args()

    no_arguments = (arguments.trash_path is None and
                    arguments.amount is None and
                    arguments.capacity is None and
                    arguments.period is None)

    if no_arguments or arguments.show:
        config.print_config(arguments.config_file)

    if arguments.amount is not None:
        try:
            config.set_amount_policy(
                value=arguments.amount,
                path_to_config=arguments.config_file,
                silent_mode=arguments.silent_mode)
        except (OSError, ValueError) as error:
            logging.error(error)
            return 1

    if arguments.capacity is not None:
        try:
            config.set_capacity_policy(
                value=arguments.capacity,
                path_to_config=arguments.config_file,
                silent_mode=arguments.silent_mode)
        except (OSError, ValueError) as error:
            logging.error(error)
            return 1

    if arguments.period is not None:
        try:
            config.set_period_policy(
                value=arguments.period,
                path_to_config=arguments.config_file,
                silent_mode=arguments.silent_mode)
        except (OSError, ValueError) as error:
            logging.error(error)
            return 1

    if arguments.trash_path is not None:
        try:
            config.move_trash(
                new_path_to_trash=arguments.trash_path,
                path_to_config=arguments.config_file,
                silent_mode=arguments.silent_mode
            )
        except OSError as error:
            logging.error(error)
            return 1

    return 0
