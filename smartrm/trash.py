# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Module for trash logic."""
import os
import logging
import pprint

import smartrm.util as util
import smartrm.logger as logger
import smartrm.config as config
import smartrm.constants as constants
import smartrm.trash_base as trash_base


def clean_files(
        filenames,
        dry_run=False,
        select_all=True,
        silent_mode=True,
        interactive=False,
        custom_trash_path=None,
        amount_policy=None,
        capacity_policy=None,
        period_policy=None,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Permanently remove files from trash bin.

    Args:
        - filenames (list): list with names of files to be removed;
        - dry_run (bool): if True, program run without any changes;
        - select_all (bool): if True, take all files if there are
        several files eith same filename;
        - silent_mode (bool): if True, program run without any output;
        - interactive (bool): if True, request permission to remove each file;
        - custom_trash_path (str): path to user trash bin;
        - amount_policy (int): max amount of elements allowed in trash bin;
        - capacity_policy (int): max size of trash bin in Mb;
        - period_policy (int): period after files are removed from
        trash bin in days;
        - path_to_config (str): path to config file;

    Raises:
        TypeError: if filenames are not list or tuple;
        ValueError: if invalid config file passed;
        OSErrr:
            - if not existing config file passed;
            - if any file cannot be removed;
    """

    if isinstance(filenames, str):
        raise TypeError('list or tuple expected')

    logger.set_logger_handlers(dry_run, silent_mode)

    config_dict = config.load_config(path_to_config)
    config.update_config(
        config_dict,
        amount_policy=amount_policy,
        capacity_policy_mb=capacity_policy,
        period_policy_days=period_policy,
        path_to_trash=custom_trash_path
    )

    path_to_trash = config_dict['path_to_trash']

    files = trash_base.load_trash_base(path_to_trash)
    files_to_clean, error_files = trash_base.get_files_by_names(
        filenames=filenames,
        path_to_trash=path_to_trash,
        select_all=select_all,
        silent_mode=silent_mode
    )

    result_info = INIT_RESULT_INFO = dict(
        errors=False,
        files=error_files
    )

    if result_info['files']:
        result_info['errors'] = True

    for cur_file in files_to_clean:
        try:
            _remove_file(
                files=files,
                dry_run=dry_run,
                interactive=interactive,
                select_all=select_all,
                path_to_trash=path_to_trash,
                file_to_clean=cur_file
            )
        except (OSError, Warning) as error:
            error_message = str(error)
            util.fill_error_info(result_info, cur_file, error_message)
        else:
            util.fill_error_info(result_info, cur_file)

    removed_files = trash_base.check_policies(files, config_dict)
    result_info['removed'] = removed_files
    trash_base.write_changes(files, path_to_trash)

    return result_info


def _remove_file(
        files,
        dry_run,
        interactive,
        path_to_trash,
        select_all,
        file_to_clean):
    """Remove file that already in trash."""
    message = 'do you want remove {} from trash?'.format(file_to_clean['name'])

    if select_all or not interactive or util.get_prompt(message):
        if not dry_run:
            util.remove_file_from_trash(
                files,
                file_to_clean,
                path_to_trash
            )
        path = os.path.join(file_to_clean['dir'], file_to_clean['name'])
        message = 'Removing {} from trash'.format(path)
        logging.info(message)


def restore_files(
        filenames,
        dry_run=False,
        select_all=True,
        silent_mode=True,
        interactive=False,
        custom_trash_path=None,
        replace=False,
        add_suffix=True,
        amount_policy=None,
        capacity_policy=None,
        period_policy=None,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Restore from trash bin.

    Args:
        - filenames (list): list with names of files to be restored;
        - dry_run (bool): if True, program run without any changes;
        - select_all (bool): if True, take all files if there are several
        files with same filename;
        - silent_mode (bool): if True, program run without any output;
        - interactive (bool): if True, request permission to remove each file;
        - custom_trash_path (str): path to user trash bin;
        - replace (bool): replace existing file if file with the same name
        already exists;
        - add_suffix (bool): rename file by adding suffix if file with the
        same name already exists;
        with the same file name already exist;
        - amount_policy (int): max amount of elements allowed in trash bin;
        - capacity_policy (int): max size of trash bin in Mb;
        - period_policy (int): period after files are removed from trash
        bin in days;
        - path_to_config (str): path to config file;

    Raises:
        TypeError: if filenames are not list or tuple;
        ValueError:
            - if invalid config file passed;
            - if both replace and add_suffix flags True;
        OSErrr:
            - if not existing config file passed;
            - if any file cannot be restored;
    """
    if isinstance(filenames, str):
        raise TypeError('list or tuple expected')

    if replace and add_suffix:
        raise ValueError('add suffix and replace cannot be set together')

    logger.set_logger_handlers(dry_run, silent_mode)

    config_dict = config.load_config(path_to_config)
    config.update_config(
        config_dict,
        amount_policy=amount_policy,
        capacity_policy_mb=capacity_policy,
        period_policy_days=period_policy,
        path_to_trash=custom_trash_path
    )
    path_to_trash = config_dict['path_to_trash']
    files = trash_base.load_trash_base(path_to_trash)

    files_to_restore, error_files = trash_base.get_files_by_names(
        filenames=filenames,
        path_to_trash=path_to_trash,
        select_all=select_all,
        silent_mode=silent_mode
    )

    result_info = INIT_RESULT_INFO = dict(
        errors=False,
        files=error_files
    )

    if result_info['files']:
        result_info['errors'] = True

    for cur_file in files_to_restore:
        try:
            _restore_file(
                files=files,
                path_to_trash=path_to_trash,
                replace=replace,
                dry_run=dry_run,
                silent_mode=silent_mode,
                interactive=interactive,
                add_suffix=add_suffix,
                file_to_restore=cur_file
            )
        except OSError as error:
            error_message = str(error)
            util.fill_error_info(result_info, cur_file, error_message)
        else:
            util.fill_error_info(result_info, cur_file)

    removed_files = trash_base.check_policies(files, config_dict)
    result_info['removed'] = removed_files
    trash_base.write_changes(files, path_to_trash)

    return result_info

    if failed_to_restore:
        raise OSError('cannot clean file(s):\n{}'.format('\n'.join(failed_to_restore)))


def _restore_file(
        files,
        path_to_trash,
        replace,
        silent_mode,
        dry_run,
        interactive,
        add_suffix,
        file_to_restore,
        create_dir=False):
    """Restore file that already in trash."""
    if silent_mode:
        create_dir = True

    original_directory = file_to_restore['dir']
    original_path = os.path.join(original_directory, file_to_restore['name'])
    path_in_trash = os.path.join(path_to_trash, file_to_restore['name'] + file_to_restore['time'])
    new_path = original_path

    if os.path.exists(new_path):
        message = '{} file or directory already exists. do you want to replace it?'.format(new_path)
        if not replace and not add_suffix:
            to_replace = util.get_prompt(message)
        elif add_suffix:
            to_replace = False
        elif replace:
            to_replace = True
        if not to_replace:
            suffix = 1
            uniq_path = new_path + ' ({suffix})'.format(suffix=suffix)
            while os.path.exists(uniq_path):
                suffix += 1
                uniq_path = new_path + ' ({suffix})'.format(suffix=suffix)
            new_path = uniq_path

    if not os.path.exists(file_to_restore['dir']):
        message = 'current directory no longer exists, do you want to restore it?'
        if create_dir or util.get_prompt(message):
            os.makedirs(original_directory)
        else:
            message = 'Not recovering {}'.format(new_path)
            logging.warning(message)

    message = 'do you want restore {} from trash?'.format(file_to_restore['name'])
    if not interactive or util.get_prompt(message):
        if not dry_run:
            util.restore_from_trash(
                files,
                file_to_restore,
                new_path,
                path_in_trash
            )

    message = 'Restoring {}'.format(os.path.join(original_path))
    logging.info(message)


def print_files_from_trash(
        custom_trash_path=None,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Print files in trash.
    Args:
        - custom_trash_path (str): path to user trash bin;
        - path_to_config (str): path to config file;

    Raises:
        ValueError: if invalid config file passed;
        OSErrr: if not existing config file passed;
    """
    config_dict = config.load_config(path_to_config)
    config.update_config(
        config_dict,
        path_to_trash=custom_trash_path
    )

    path_to_trash = config_dict['path_to_trash']
    files = trash_base.load_trash_base(path_to_trash)

    logger.set_logger_handlers(silent=False)

    trash_base.check_policies(files, config_dict)
    trash_base.write_changes(files, path_to_trash)

    if files:
        trash_base.print_files(files)
    else:
        message = 'Trash is empty.'
        print message


def get_files(
        trash_path=None,
        amount_policy=None,
        capacity_policy=None,
        period_policy=None,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Return list of filedicts in trash.
    Args:
        - trash_path (str): path to user trash bin;
        - amount_policy (int): max amount of elements allowed in trash bin;
        - capacity_policy (int): max size of trash bin in Mb;
        - period_policy (int): period after files are removed from trash
        bin in days;
        - path_to_config (str): path to config file;

    Raises:
        ValueError: if invalid config file passed;
        OSErrr: if not existing config file passed;

    Returns:
        list: files in trash bin';;
    """
    config_dict = config.load_config(path_to_config)

    config.update_config(
        config_dict,
        amount_policy=amount_policy,
        capacity_policy_mb=capacity_policy,
        period_policy_days=period_policy,
        path_to_trash=trash_path
    )

    path_to_trash = config_dict['path_to_trash']

    return trash_base.load_trash_base(path_to_trash)
