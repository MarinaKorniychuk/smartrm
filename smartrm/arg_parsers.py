# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Module with argument parsers for diffferent modes."""
import argparse
import os

import smartrm.constants as constants


def rm_parse_args():
    """Parse arguments for rm mode.

    Returns:
        argparse.Namespace: passed arguments.
    """
    parser_rm = argparse.ArgumentParser(
        description='Mode for moving files to trash.')

    parser_rm.add_argument(
        '-f', '--force',
        dest='rm_force',
        action='store_true',
        help='ignore nonexistent files and arguments,never prompt'
    )
    parser_rm.add_argument(
        '-d', '--dir',
        dest='rm_directory',
        action='store_true',
        help='remove empty directories'
    )
    parser_rm.add_argument(
        '-r', '-R', '--recursive',
        dest='rm_recursive',
        action='store_true',
        help='remove directories and their contents recursively'
    )
    parser_rm.add_argument(
        '--dry-run',
        action='store_true',
        help='run without making any changes'
    )
    parser_rm.add_argument(
        '-t', '--trash-path',
        metavar='DIRECTORY',
        default=None,
        help='directory for trash location'
    )
    parser_rm.add_argument(
        '--period_policy',
        dest='period',
        metavar='DAYS',
        type=int,
        default=None,
        nargs='?',
        help='determine how long (in DAYS) files stored in trash. '
             'if nothing or 0 passed, reset storage period policy'
    )
    parser_rm.add_argument(
        '--capacity-policy',
        dest='capacity',
        metavar='MB',
        type=int,
        default=None,
        nargs='?',
        help='determine max value of trash in MB. '
             'if nothing or 0 passed, reset capacity policy'
    )
    parser_rm.add_argument(
        '--amount-policy',
        dest='amount',
        metavar='amount',
        type=int,
        default=None,
        nargs='?',
        help='determine max amount of files in trash. '
             'if nothing or 0 passed, reset amount policy'
    )
    parser_rm.add_argument(
        '--config',
        metavar='CONFIG_FILE',
        default=constants.CONFIG_PATH_DEFAULT,
        help='start program with custom CONFIG file'
    )

    files_group = parser_rm.add_mutually_exclusive_group(required=True)
    files_group.add_argument(
        'files',
        metavar='FILE',
        nargs='*',
        default=[],
        help='files to remove'
    )
    files_group.add_argument(
        '--regexp',
        metavar=('PATTERN', 'SUBTREE'),
        nargs=2,
        help='remove files and directories in passed SUBTREE by PATTERN'
    )

    silent_or_interact_group = parser_rm.add_mutually_exclusive_group(
        required=False
    )
    silent_or_interact_group.add_argument(
        '--silent',
        action='store_true',
        dest='silent_mode',
        help='run without logging (--force flag will be added)'
    )
    silent_or_interact_group.add_argument(
        '-i',
        dest='rm_interactive',
        action='store_true',
        help='prompt before every removal'
    )

    arguments = parser_rm.parse_args()
    arguments.config = os.path.realpath(arguments.config)

    if arguments.silent_mode:
        arguments.rm_force = True
    return arguments


def trash_parse_args():
    """Parser for trash mode.

    Returns:
        argparse.Namespace: passed arguments.
    """

    parser_trash = argparse.ArgumentParser(
        description='Mode for working with files that alreade in trash.'
    )

    parser_trash.add_argument(
        '-s', '--show',
        action='store_true',
        help='show files in trash'
    )
    parser_trash.add_argument(
        '--all',
        action='store_true',
        dest='select_all',
        help=('select all files '
              'if there are several files with passed name in trash')
    )
    parser_trash.add_argument(
        '--replace',
        action='store_const',
        dest='replace',
        const='False',
        help='replace existinf file if file with such name already exists'
    )
    parser_trash.add_argument(
        '-c', '--clean',
        metavar='FILE',
        nargs='*',
        help=('permanently delete passed files or delete '
              'all files if no agruments passed')
    )
    parser_trash.add_argument(
        '-r', '--restore',
        metavar='FILE',
        nargs='*',
        help=('restore recently deleted files or restore '
              'all files if no arguments passed')
    )
    parser_trash.add_argument(
        '--dry-run',
        action='store_true',
        help='run without making any changes'
    )
    parser_trash.add_argument(
        '-t', '--trash-path',
        metavar='DIRECTORY',
        default=None,
        help='directory for trash location'
    )
    parser_trash.add_argument(
        '--period-policy',
        dest='period',
        metavar='DAYS',
        type=int,
        default=None,
        nargs='?',
        help='determine how long (in DAYS) files stored in trash. '
             'if nothing or 0 passed, reset storage period policy'
    )
    parser_trash.add_argument(
        '--capacity-policy',
        dest='capacity',
        metavar='MB',
        type=int,
        default=None,
        nargs='?',
        help='determine max value of trash in MB. '
             'if nothing or 0 passed, reset capacity policy'
    )
    parser_trash.add_argument(
        '--amount-policy',
        dest='amount',
        metavar='amount',
        type=int,
        default=None,
        nargs='?',
        help='determine max amount of files in trash. '
             'if nothing or 0 passed, reset amount policy'
    )
    parser_trash.add_argument(
        '--config',
        metavar='CONFIG_FILE',
        default=constants.CONFIG_PATH_DEFAULT,
        help='start program with custom CONFIG file'
    )

    silent_or_interact_group = parser_trash.add_mutually_exclusive_group(
        required=False
    )
    silent_or_interact_group.add_argument(
        '--silent',
        action='store_true',
        dest='silent_mode',
        help='run without logging (--force flag will be added)'
    )
    silent_or_interact_group.add_argument(
        '-i',
        dest='rm_interactive',
        action='store_true',
        help='prompt before every removal'
    )

    arguments = parser_trash.parse_args()
    arguments.config = os.path.realpath(arguments.config)

    arguments.add_suffix = False
    if arguments.silent_mode:
        arguments.select_all = True
        if not arguments.replace:
            arguments.add_suffix = True

    return arguments


def config_parse_args():
    """Parser for config mode.

    Returns:
        argparse.Namespace: passed arguments.
    """
    parser_config = argparse.ArgumentParser(
        description='Mode for work with trash configurations.'
    )

    parser_config.add_argument(
        '--config-file',
        metavar='CONFIG',
        default=constants.CONFIG_PATH_DEFAULT,
        help='work with configurations from passed CONFIG file'
    )
    parser_config.add_argument(
        '--silent',
        action='store_true',
        dest='silent_mode',
        help='run without logging'
    )
    parser_config.add_argument(
        '-s', '--show',
        action='store_true',
        help='print config file'
    )
    parser_config.add_argument(
        '-t', '--trash-path',
        metavar='DIRECTORY',
        help='directory for new trash location'
    )
    parser_config.add_argument(
        '--period-policy',
        dest='period',
        metavar='DAYS',
        type=int,
        nargs='?',
        const=0,
        help='determine how long (in DAYS) files stored in trash. '
             'if nothing or 0 passed, reset storage period policy'
    )
    parser_config.add_argument(
        '--capacity-policy',
        dest='capacity',
        metavar='MB',
        type=int,
        nargs='?',
        const=0,
        help='determine max value of trash in MB. '
             'if nothing or 0 passed, reset capacity policy'
    )
    parser_config.add_argument(
        '--amount-policy',
        dest='amount',
        metavar='AMOUNT',
        type=int,
        nargs='?',
        const=0,
        help='determine max amount of files in trash. '
             'if nothing or 0 passed, reset amount policy'
    )

    arguments = parser_config.parse_args()
    arguments.config_file = os.path.realpath(arguments.config_file)

    return arguments
