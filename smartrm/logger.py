# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Create and manage program logger."""

import sys
import logging

import smartrm.constants as constants


def set_logger_handlers(dry_run=False, silent=True):
    """Set appropriete handlers to program logger.

    Args:
        - dry_run (bool): if True, add only Stream Handler to logger;
        - silent_mode (bool): if True, add only File Handler to logger;
    """
    logger = logging.getLogger()
    if silent:
        set_silent_mode()

    if dry_run:
        set_dry_run_mode()
        logging.warning("dry mode: changes won't be applied ")

    if not silent and not dry_run:
        add_handlers()



def get_stream_handler():
    """Create Stream handler for logger.

    Returns:
        logging.StreamHandler: handler with necessary level and formatter;
    """
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    stream_formatter = logging.Formatter('%(levelname)s: %(message)s')
    stream_handler.setFormatter(stream_formatter)
    return stream_handler


def get_file_handler():
    """Create File handler for logger.

    Returns:
        logging.FileHandler: handler with necessary level and formatter;
    """
    file_handler = logging.FileHandler(constants.LOG_PATH)
    file_handler.setLevel(logging.DEBUG)
    format_string = '%(levelname)s: %(message)s - %(asctime)s'
    file_formatter = logging.Formatter(format_string)
    file_handler.setFormatter(file_formatter)
    return file_handler


def create_logger():
    """Create logger."""
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)


def set_silent_mode():
    """Add only File handler."""
    logger = logging.getLogger()
    if all(not isinstance(handler, logging.FileHandler)
            for handler in logger.handlers):
        file_handler = get_file_handler()
        logger.addHandler(file_handler)


def set_dry_run_mode():
    """Add only Stream handler."""
    logger = logging.getLogger()
    if all(not isinstance(handler, logging.StreamHandler)
            for handler in logger.handlers):
        stream_handler = get_stream_handler()
        logger.addHandler(stream_handler)


def add_handlers():
    """Add Stream and File Handlers to logger."""
    logger = logging.getLogger()
    stream_handler = get_stream_handler()
    file_handler = get_file_handler()
    if all(not isinstance(handler, logging.StreamHandler)
            for handler in logger.handlers):
        logger.addHandler(stream_handler)
    if all(not isinstance(handler, logging.FileHandler)
            for handler in logger.handlers):
        logger.addHandler(file_handler)
