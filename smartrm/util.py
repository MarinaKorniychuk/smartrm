# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Aditional util for program."""

import os
import datetime


# ========== Work with files ==========

def move_file_to_trash(
        real_path,
        path_to_trash,
        file_info):
    """Move file to trash.

    Args:
        - real_path (str): full path to file;
        - path_to_trash (str): path to trash bin;
        - file_infp (dict): dict with file info;
    """
    new_file_name = file_info['name'] + file_info['time']
    new_path = os.path.join(path_to_trash, new_file_name)

    os.rename(real_path, new_path)


def remove_file_from_trash(
        files,
        file_to_clean,
        path_to_trash):
    """Remove file from trash permanently.

    Args:
        - files (list): files in trash bin as dicts with info;
        - file_to_clean (str): full path to file;
        - path_to_trash (str): path to trash bin;

    Raises:
        OSError: if file or directory not exists;
    """
    filename = file_to_clean['name'] + file_to_clean['time']
    path_in_trash = os.path.join(path_to_trash, filename)

    if not os.path.exists(path_in_trash):
        raise OSError('smartrm: cannot remove {}. no such file or directory'.format(path_in_trash))

    if os.path.isdir(path_in_trash):
        remove_dir(path_in_trash)
    else:
        os.remove(path_in_trash)

    files.remove(file_to_clean)


def restore_from_trash(
        files,
        files_to_restore,
        new_path,
        path_in_trash):
    """Restore file to original location.
    R
    Args:
        - files (list): files in trash bin as dicts with info;
        - files_to_restore (str): full path to file;
        - new_path (str): new path for file;
        - path_to_trash (str): path to trash bin;

    Raises:
        OSError: if file or directory not exists;
    """
    if not os.path.exists(path_in_trash):
        raise OSError('no such file or directory')

    os.rename(path_in_trash, new_path)

    files.remove(files_to_restore)


# ==================================================

def get_prompt(message):
    """Print message for user and read answer (y/n).

    Args:
        message(str): message for user;

    Returns:
        bool: answer;
    """
    message = 'smartrm: ' + message + ' y/n: '
    answer = raw_input(message).lower()
    if answer[0] == 'y':
        return True
    if answer[0] == 'n':
        return False


def check_index(length, index):
    """Check whether index is valid.

    Args:
        - length (int): amount of files in trash bin;
        - index (int): index to validate;

    Raises:
        OSError: if index is invalid;

    Returns:
        bool: True if index is correct;
    """
    if range(0, length+1).count(index) or index == '':
        return True
    raise OSError('smartrm: wrong index')


def remove_dir(path_to_directory):
    """Recursively remove directory.

    Args:
        - path_to_directory (str): path to directory to remove;
    """
    for dirpath, _, filenames in os.walk(path_to_directory):
        for filename in filenames:
            os.remove(os.path.join(dirpath, filename))
        os.rmdir(dirpath)


def create_trash(path_to_trash):
    """ Create trash in passed path.

    Args:
        - path_to_trash (str): path to trash bin;
    """
    os.makedirs(path_to_trash)


def get_file_info(path_to_file):
    """Return dict with infromation about file.

    Args:
        - path_to_file (str): path to file;

    Returns:
        dict: dict with info about file:
            - name
            - parent directory;
            - current time;
            - type (file/directory);
            - size
    """
    fileinfo = dict()
    file_type = 'DIR' if os.path.isdir(path_to_file) else 'FILE'

    fileinfo['name'] = os.path.basename(path_to_file)
    fileinfo['dir'] = os.path.dirname(path_to_file)
    fileinfo['time'] = str(datetime.datetime.now())
    fileinfo['type'] = file_type
    try:
        fileinfo['size'] = os.path.getsize(path_to_file)
    except OSError:
        fileinfo['size'] = None
    return fileinfo


def fill_error_info(result_info, file_info, error_message=''):

    file_info['error_message'] = error_message
    if error_message:
        result_info['errors'] = True
        file_info['error'] = True
    else:
        error_message = ''
        file_info['error'] = False

    result_info['files'].append(file_info)
