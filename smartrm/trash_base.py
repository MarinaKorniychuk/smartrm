# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Module for work with trash base."""
import os
import json
import logging
import datetime

import smartrm.constants as constants

import smartrm.util as util


def check_policies(files, config_dict):
    """Check each policy for files in trash base.

    Args:
        - files (list): files in trash bin as dicts with info;
        - config_dict (dict): dict with config parametrs;
    """
    removed_files = []
    amount_value = config_dict['amount_policy']
    size_value = config_dict['capacity_policy_mb']
    time_value = config_dict['period_policy_days']
    path_to_trash = config_dict['path_to_trash']
    removed_files.extend(check_capacity_policy(size_value, files, path_to_trash))
    removed_files.extend(check_amount_policy(amount_value, files, path_to_trash))
    removed_files.extend(check_period_policy(time_value, files, path_to_trash))
    return removed_files


def check_amount_policy(amount, files, path_to_trash):
    """Remove files until trash contains accepted amount of files.

    Args:
        - amount (int): allowed amount of files in trash bin;
        - files (list): files in trash bin as dicts with info;
        - path_to_trash (str): path to trash bin;
    """
    removed_files = []
    amount_to_remove = len(files) - amount
    for _ in range(amount_to_remove):
        removed_files.append([files[0]['name'], files[0]['time']])
        util.remove_file_from_trash(
            files,
            files[0],
            path_to_trash
        )
    return removed_files


def check_capacity_policy(size_value, files, path_to_trash):
    """Remove files until size of files greater than capacity policy value.

    Args:
        - capacity (int): allowed aize of files in trash bin (in Mb);
        - files (list): files in trash bin as dicts with info;
        - path_to_trash (str): path to trash bin;
    """
    removed_files = []
    size_value *= 1024**2
    sum_size = sum([filedict['size'] for filedict in files])

    while sum_size > size_value and files:
        sum_size -= files[0]['size']
        removed_files.append([files[0]['name'], files[0]['time']])
        util.remove_file_from_trash(
            files,
            files[0],
            path_to_trash
        )
    return removed_files


def check_period_policy(time_value, files, path_to_trash):
    """Remove files according to period policy value.

    Args:
        - period (int): period after that files removed from trash bin;
        - files (list): files in trash bin as dicts with info;
        - path_to_trash (str): path to trash bin;
    """
    removed_files = []
    for file_dict in files[:]:
        remov_data = datetime.datetime.strptime(
            file_dict['time'],
            '%Y-%m-%d %H:%M:%S.%f'
        )
        cur_time = datetime.datetime.now()

        if (cur_time - remov_data).days > time_value:
            removed_files.append([file_dict['name'], file_dict['time']])
            util.remove_file_from_trash(
                files,
                file_dict,
                path_to_trash
            )
    return removed_files


def _create_trash_base(path_to_trash_base):
    """Initialize trash base."""
    trash_base_init = constants.INIT_FILES
    with open(path_to_trash_base, 'w') as outfile:
        json.dump(dict(files=trash_base_init), outfile, indent=4)


def write_changes(files, path_to_trash):
    """Write changed info about files in trash.

    Args:
        - files (list): files in trash bin as dicts with info;
        - path_to_trash (str): path to trash bin;
    """
    path_to_trash_base = os.path.join(path_to_trash, constants.TRASH_BASE_NAME)
    trash_base_data = dict(files=files)
    with open(path_to_trash_base, 'w') as outfile:
        json.dump(trash_base_data, outfile, indent=4)


def get_files(path_to_trash=constants.TRASH_PATH_DEFAULT):
    """Return files in trash bin.

    Args:
        - path_to_trash (str): path to trash bin;

    Returns:
        list: list with files as dicts with info;
    """
    path_to_trash_base = os.path.join(path_to_trash, constants.TRASH_BASE_NAME)
    with open(path_to_trash_base, 'r') as trash_base_file:
        files = json.load(trash_base_file)
    return files['files'][:]


def print_files(files):
    """Print selected files.

    Args:
        - files (list): files in trash bin as dicts with info;
    """
    max_index_len = len(str(len(files)))

    size_len = max([len(str(x['size'])) for x in files])

    printed_list = []
    for i, cur_file in enumerate(files):
        printed_list.append(
            '{index:>{n}}. {type:>5} {size:>{s}}  {date}  {dir}/{name}'
            .format(
                index=i+1,
                type=cur_file['type'],
                size=cur_file['size'],
                date=cur_file['time'].split('.')[0],
                dir=cur_file['dir'],
                name=cur_file['name'],
                n=max_index_len,
                s=size_len)
            )
    files_list_str = '\n'.join(printed_list)
    if len(printed_list) < 30:
        print files_list_str
    else:
        from pydoc import pager
        pager(files_list_str)


def get_files_by_names(
        filenames,
        select_all=False,
        silent_mode=True,
        path_to_trash=constants.TRASH_PATH_DEFAULT):
    """Return list of files with passed filenames.

    Args:
        - filenames (list): filenames to search file;
        - select_all (bool): is True, take all files with tha same name;
        - silent_mode (bool): if True, run without any output;
        - path_to_trash (str): path to trash bin;

    Returns:
        list: list with selected files;
    """

    message = 'do you want to select all files in trash?'

    if not filenames and (select_all or util.get_prompt(message)):
        return (get_files(path_to_trash), [])

    result_files = []
    error_files = []
    for filename in filenames:
        if isinstance(filename, basestring):
            selected_by_filename = _get_all_files_by_name(
                filename=filename,
                path_to_trash=path_to_trash,
            )
        else:
            selected_by_filename = _get_files_by_timestamp(
                timestamp=filename[0],
                filename=filename[1],
                path_to_trash=path_to_trash
            )

        if len(selected_by_filename) == 0:
            if isinstance(filename, basestring):
                file_info(name=filename)
            else:
                file_info = dict(
                    time=filename[0],
                    name=filename[1]
                )
            error = 'there is no such file or directory in trash'
            file_info['error'] = True
            file_info['error_message'] = error
            error_files.append(file_info)

        if len(selected_by_filename) > 1 and not select_all:
            selected_by_filename = get_prompt_file_index(selected_by_filename)

        result_files.extend(selected_by_filename)
    return (result_files, error_files)


def get_prompt_file_index(files):
    """Check and returt index passed by user.

    Args:
        - files (list): files in trash bin as dicts with info;

    Raises:
        ValueError: if invalid input;

    Returns:
        list: list with selected files;
    """
    message = 'smartrm: there are several files with such name, '
    prop = '\nchoose file, enter nothing to choose all or 0 to choose nothing: '
    print_files(files)
    ind = raw_input(message + prop)
    try:
        ind = int(ind)
    except ValueError:
        pass

    if util.check_index(len(files), ind):
        if ind == '':
            return files
        if ind == 0:
            return []
        return [files[ind - 1]]


def _get_all_files_by_name(
        filename,
        path_to_trash=constants.TRASH_PATH_DEFAULT):
    """Return all files with such filename in list."""
    files = get_files(path_to_trash)
    return [cur_file for cur_file in files if cur_file['name'] == filename]


def _get_files_by_timestamp(
        timestamp,
        filename,
        path_to_trash=constants.TRASH_PATH_DEFAULT):
    """Return all files with such timestamp in list."""
    files = get_files(path_to_trash)
    return [cur_file for cur_file in files
                    if cur_file['time'] == timestamp
                    and cur_file['name'] == filename]


def _check_trash_exists(path_to_trash, path_to_trash_base):
    """Check trash base existence."""
    if not os.path.exists(path_to_trash):
        util.create_trash(path_to_trash)

    if not os.path.exists(path_to_trash_base):
        _create_trash_base(path_to_trash_base)


def load_trash_base(path_to_trash=constants.TRASH_PATH_DEFAULT):
    """Load dicts with info about files in trash.

    Args:
        - path_to_trash (str): path to trash bin;

    Returns:
        list: list with files in trash bin;
    """
    path_to_trash_base = os.path.join(path_to_trash, constants.TRASH_BASE_NAME)
    _check_trash_exists(path_to_trash, path_to_trash_base)

    with open(path_to_trash_base) as trash_base_file:
        files = json.load(trash_base_file)['files']

    return files
