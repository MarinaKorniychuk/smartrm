# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Constants used in program."""
from os.path import expanduser, join

PROGRAM_NAME = 'smartrm'
CONFIG_NAME = '.' + PROGRAM_NAME + '_config.json'
TRASH_BASE_NAME = '.' + PROGRAM_NAME + '_files.json'

CONFIG_PATH_DEFAULT = join(expanduser('~'), CONFIG_NAME)
TRASH_PATH_DEFAULT = join(expanduser('~'), '.' + PROGRAM_NAME)
TRASH_FILES_BASE_PATH = join(TRASH_PATH_DEFAULT, TRASH_BASE_NAME)
LOG_PATH = join(expanduser('~'), '.smartrm_log')

INIT_CONFIG = dict(
    path_to_trash=TRASH_PATH_DEFAULT,
    period_policy_days=60,
    capacity_policy_mb=512,
    amount_policy=100
)

INIT_FILES = []

POLICIES = ['period_policy_days', 'capacity_policy_mb', 'amount_policy']

INIT_RESULT_INFO = dict(
        errors=False,
        files=[]
    )
