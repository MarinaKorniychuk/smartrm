# /usr/bin/env python2.7
# -*- coding: utf-8 -*-

""" Module for rm mode logic."""

import os
import re
import logging
import functools
import multiprocessing

import smartrm.util as util
import smartrm.logger as logger
import smartrm.config as config
import smartrm.constants as constants
import smartrm.trash_base as trash_base


def remove_files(
        filenames,
        dry_run=False,
        rm_force=True,
        silent_mode=True,
        rm_directory=True,
        rm_recursive=True,
        rm_interactive=False,
        custom_trash_path=None,
        amount_policy=None,
        capacity_policy=None,
        period_policy=None,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Move files to trash.

    Args:
        - filenames (list): list with names of files to be removed;
        - dry_run (bool): if True, program run without any changes;
        - rm_force (bool): if True, remove write-protected files;
        - silent_mode (bool): if True, program run without any output;
        - rm_directore (bool): if True, remove empty directory;
        - rm_recursive (bool): if True, remove non empty directories;
        - rm_interactive (bool): if True, request permission to remove each file;
        - custom_trash_path (str): path to user trash bin;
        - amount_policy (int): max amount of elements allowed in trash bin;
        - capacity_policy (int): max size of trash bin in Mb;
        - period_policy (int): period after files are removed from
        trash bin in days;
        - path_to_config (str): path to config file;

    Raises:
        TypeError: if filenames are not list or tuple;
        ValueError: if invalid config file passed;
        OSErrr:
            - if not existing coonfig passed;
            - if any file cannot be removed;
    """

    if isinstance(filenames, str):
        raise TypeError('list or tuple expected')

    logger.set_logger_handlers(dry_run, silent_mode)

    config_dict = config.load_config(path_to_config)
    config.update_config(
        config_dict,
        period_policy_days=period_policy,
        capacity_policy_mb=capacity_policy,
        amount_policy=amount_policy,
        path_to_trash=custom_trash_path
    )
    path_to_trash = config_dict['path_to_trash']
    files = trash_base.load_trash_base(path_to_trash)

    result_info = dict(
        errors=False,
        files=[]
    )

    partial_remove_file = functools.partial(
        _remove_file,
        dry_run=dry_run,
        rm_force=rm_force,
        rm_directory=rm_directory,
        rm_recursive=rm_recursive,
        rm_interactive=rm_interactive,
        path_to_trash=path_to_trash,
        path_to_config=path_to_config
    )
    pool = multiprocessing.Pool()
    try:
        result_info['files'] = pool.map(partial_remove_file, filenames)
    finally:
        pool.close()
        pool.join()


    if any(file_info['error'] for file_info in result_info['files']):
        result_info['errors'] = True

    for file_info in result_info['files']:
        if not file_info['error'] and not dry_run:
            files.append(file_info)

    removed_files = trash_base.check_policies(files, config_dict)
    result_info['removed'] = removed_files
    trash_base.write_changes(files, path_to_trash)

    return result_info


def remove_by_regexp(
        pattern,
        subtree,
        dry_run=False,
        rm_force=True,
        silent_mode=True,
        rm_directory=True,
        rm_recursive=True,
        rm_interactive=False,
        custom_trash_path=None,
        period_policy=None,
        capacity_policy=None,
        amount_policy=None,
        path_to_config=constants.CONFIG_PATH_DEFAULT):
    """Move files to trash by pattern in a subtree.

    Args:
        - pattern (str): regular expression for filenames;
        - subtree (str): path to subtree where files will be removed;
        - dry_run (bool): if True, program run without any changes;
        - rm_force (bool): if True, remove write-protected files;
        - silent_mode (bool): if True, program run without any output;
        - rm_directore (bool): if True, remove empty directory;
        - rm_recursive (bool): if True, remove non empty directories;
        - rm_interactive (bool): if True, request permission to remove each file;
        - custom_trash_path (str): path to user trash bin;
        - amount_policy (int): max amount of elements allowed in trash bin;
        - capacity_policy (int): max size of trash bin in Mb;
        - period_policy (int): period after files are removed from
        trash bin in days;
        - path_to_config (str): path to config file;

    Raises:
        ValueError:
            - if invalid config file passed;
            - if regular expression is invalid;
        OSErrr:
            - if not existing coonfig passed;
            - if any file cannot be removed;
    """
    logger.set_logger_handlers(dry_run, silent_mode)

    config_dict = config.load_config(path_to_config)
    config.update_config(
        config_dict,
        period_policy_days=period_policy,
        capacity_policy_mb=capacity_policy,
        amount_policy=amount_policy,
        path_to_trash=custom_trash_path
    )
    path_to_trash = config_dict['path_to_trash']
    files = trash_base.load_trash_base(path_to_trash)

    result_info = dict(
        errors=False,
        files=[]
    )

    try:
        regexp = re.compile(pattern)
    except Exception:
        raise ValueError('passed string is not a valid regular expression')

    if not os.path.exists(subtree) or not os.path.isdir(subtree):
        raise OSError('{}: directory does not exist or it is not a directory'.format(subtree))

    for dirpath, dirnames, filenames in os.walk(subtree, topdown=False):
        directory_path = os.path.realpath(dirpath)
        for name in filenames + dirnames:
            if regexp.search(name):
                real_path = os.path.join(directory_path, name)
                file_info = _remove_file(
                    filename=real_path,
                    dry_run=dry_run,
                    rm_force=rm_force,
                    rm_directory=rm_directory,
                    rm_recursive=rm_recursive,
                    rm_interactive=rm_interactive,
                    path_to_trash=path_to_trash,
                    path_to_config=path_to_config,
                )
                result_info['files'].append(file_info)

    if any(file_info['error'] for file_info in result_info['files']):
        result_info['errors'] = True

    for file_info in result_info['files']:
        if not file_info['error'] and not dry_run:
            files.append(file_info)

    removed_files = trash_base.check_policies(files, config_dict)
    result_info['removed'] = removed_files
    trash_base.write_changes(files, path_to_trash)

    return result_info


def _remove_file(
        filename,
        dry_run,
        rm_force,
        rm_directory,
        rm_recursive,
        rm_interactive,
        path_to_trash,
        path_to_config):
    """Remove file or directory."""
    real_path = os.path.abspath(filename)
    file_info = util.get_file_info(real_path)

    try:
        _check_if_can_remove(
            real_path,
            rm_force,
            rm_recursive,
            rm_directory,
            path_to_trash,
            path_to_config
        )
    except OSError as error:
        file_info['error_message'] = error
        file_info['error'] = True
        return file_info

    message = 'move {} to trash?'.format(real_path)
    if not rm_interactive or util.get_prompt(message):
        if not dry_run:
            util.move_file_to_trash(
                real_path,
                path_to_trash,
                file_info
            )

        file_info['error_message'] = ''
        file_info['error'] = False

        message = 'Moving {} to trash'.format(real_path)
        logging.info(message)

    return file_info

def _check_if_can_remove(
        real_path,
        rm_force,
        rm_recursive,
        rm_directory,
        path_to_trash,
        path_to_config):
    """Check whether moving to trash can be done.

    Raises:
        OSError:
            - if path does not exist;
            - if path to config passed;
            - if path is inside trash bin;
            - if path to write-protected file and rm_force is False;
            - if path is a non-empty directory and rm_recursive is False;
            - if path is an empty directory and rm_force and rm_directory
            are False.
    """
    is_directory = os.path.isdir(real_path)

    if real_path == path_to_config:
        raise OSError('cannot remove config file')

    if real_path.startswith(path_to_trash):
        raise OSError('cannot remove trash files and directories')
    os.access_error = OSError(
        'cannot remove {}. '
        'Permission denied.'.format(real_path)
    )

    if not os.path.exists(real_path):
        raise OSError(
            'cannot remove {}. '
            'No such file or directory.'.format(real_path))

    if not is_directory and not os.access(real_path, os.W_OK) and not rm_force:
        message = 'remove write-protected file {}'.format(real_path)
        if not util.get_prompt(message):
            return

    if is_directory:
        is_empty = not bool(os.listdir(real_path))

        if not rm_directory and not rm_recursive:
            raise OSError(
                'cannot remove {}. '
                'Is a directory.'.format(real_path))

        if not rm_recursive and not is_empty:
            raise OSError(
                'cannot remove {}. '
                'Directory not empty.'.format(real_path))

        if not rm_force:
            for dirpath, _, filenames in os.walk(real_path):
                if not os.access(dirpath, os.W_OK):
                    raise os.access_error
                for filename in filenames:
                    if not os.access(os.path.join(dirpath, filename), os.W_OK):
                        raise os.access_error
